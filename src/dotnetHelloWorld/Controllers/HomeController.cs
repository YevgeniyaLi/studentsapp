﻿using System.Collections.Generic;
using dotnetHelloWorld.Models;
using Microsoft.AspNetCore.Mvc;

namespace dotnetHelloWorld.Controllers
{
    public class HomeController : Controller
    {
        private readonly StudentsDB studentsDb;
        
        public HomeController()
        {
            this.studentsDb = StudentsDB.instance();
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        
        public string Welcome()
        {
            return "This is the Welcome action method...";
        }
        
        public IActionResult Students()
        {
            List<Student> students = studentsDb.getStudents();
            return View(students);
        }
        
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
        
            var student = studentsDb.getStudent(id);
            if (student == null)
            {
                return NotFound();
            }
        
            return View(student);
        }
    }
}
