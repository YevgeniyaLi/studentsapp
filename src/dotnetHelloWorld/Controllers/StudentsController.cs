﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using dotnetHelloWorld.Models;
using System.Text.Encodings.Web;

namespace dotnetHelloWorld.Controllers
{
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {
        private readonly HtmlEncoder _htmlEncoder;
        private readonly StudentsDB studentsDb;

        public StudentsController(HtmlEncoder htmlEncoder)
        {
            _htmlEncoder = htmlEncoder;
            this.studentsDb = StudentsDB.instance();
        }

        // POST api/values
        [HttpPost]
        public IEnumerable<string> Post([FromBody]Student student)
        {
            string result = studentsDb.addStudent(student);
            string greeting = _htmlEncoder.Encode("Hello " + student.Name);
            return new[] {greeting, result};
        }
        
    }
}
