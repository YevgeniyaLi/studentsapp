using System.Collections.Generic;

namespace dotnetHelloWorld.Models
{
    public class StudentsDB
    {
        readonly List<Student> students;
        private static StudentsDB db;

        private StudentsDB()
        {
            if (this.students == null)
            {
                this.students = new List<Student>();
            }
        }

        public static StudentsDB instance()
        {
            if (db == null)
            {
                db = new StudentsDB();
            }

            return db;
        }

        public string addStudent(Student student)
        {
            if (!this.students.Contains(student))
            {
                this.students.Add(student);
            }

            return $"There are {students.Count} students in system";
        }

        public Student getStudent(int? id)
        {
            return students.Find(it => it.Id == id);
        }

        public List<Student> getStudents()
        {
            return this.students;
        }
    }
}